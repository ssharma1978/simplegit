# a super simple example class to use git in ruby
# change#1 related to issue54
# Removed invalid default value
# FeatureA change
# featureB changes
# featureBee changes
# featureA sandeep change#2
# featureA changes from John
# featureB sandeeo change#3
# featureB sandeep change#1 01 june 2016
class SimpleGit
  
  def initialize(git_dir = '.')
    @git_dir = File.expand_path(git_dir)
  end
  
  def show(treeish = 'master')
    command("git show #{treeish}")
  end

  private
  
    def command(git_cmd)
      Dir.chdir(@git_dir) do
        return `#{git_cmd} 2>&1`.chomp
      end
    end
  
end
